module.exports = {
  people: getPeople()
};

function getPeople() {
  return [
    {
      id: 1,
      firstName: 'John',
      middleName: 'Doe',
      lastName: 'Papa',
      gender: 'male',
      dBirth: 'January 2, 1997',
      age: 18,
      street: '1-b',
      barangay: 'Gulang Gulang',
      city: 'Lucena',
      province: 'Quezon',
      cStatus: 'Single',
      nationality: 'Pilipino',
      religion: 'Roman Catholic',

      email: 'abcd@gmail.com',
      mobileNumber: '123123123',
      phoneNumber: '123123123',

      employType: 'Full Time',

      emergencyName: 'John Doe',
      emergencyContact: '123123123',
      emergencyStreet: '1-b',
      emergencyBarangay: 'Dalahican',
      emergencyCity: 'Lucena',
      emergencyProvince: 'Quezon',

      cardName: 'Philhealth',
      cardNumber: '123123-123123-123123',

      tertiaryInstitution: 'Manuel S. Enverga University Foundation',
      course: 'Bachelor of Science in Information Technology',
      major: 'Business Analytics',
      degreeLvl: 'Bachelors Degree',
      tertiaryInstituteStreet: '1-b',
      tertiaryInstituteBarangay: 'Dalahican',
      tertiaryInstituteCity: 'Lucena',
      tertiaryInstituteProvince: 'Quezon',
      tertiaryYearGraduated: '2017',
      instituteAchieve: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not onl',

      nameOfSkill: 'Photoshop',
      proficiency: 'Proficient',
      remarksSkill: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',

      industry: 'Information Technology',
      company: 'Moon Macro System',
      salary: '39,000-50,000',
      dateStarted: 'May,2002',
      dateEnd: 'March,2006',
      reasonLeaving: 'Career Change',
      workDescription: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',

      courseCertificate: 'Photoshop seminar',
      dateAcquired: 'January 4,2014',
      facilitatedBy: 'Madelyn Green',
      certificateDesc: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',


    }/*,
    { id: 2, firstName: 'Ward', lastName: 'Bell', age: 31, location: 'California' },
    { id: 3, firstName: 'Colleen', lastName: 'Jones', age: 21, location: 'New York' },
    { id: 4, firstName: 'Madelyn', lastName: 'Green', age: 18, location: 'North Dakota' },
    { id: 5, firstName: 'Ella', lastName: 'Jobs', age: 18, location: 'South Dakota' },
    { id: 6, firstName: 'Landon', lastName: 'Gates', age: 11, location: 'South Carolina' },
    { id: 7, firstName: 'Haley', lastName: 'Guthrie', age: 35, location: 'Wyoming' },
    { id: 8, firstName: 'Aaron', lastName: 'Jinglehiemer', age: 22, location: 'Utah' }*/



  ];
}
