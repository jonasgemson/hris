/**
 * Created by Mark on 3/20/2017.
 */
(function () {
  'use strict';

  var app = angular.module('app.directives');

  // app.controller('NameCtrl', function () {
  //   var vm = this;
  //   vm.namePattern =  '/^[a-zA-Z ]{1,25}$/';
  // });

  app.directive('lnameDirective', function () {
    return {
      restrict: 'AE',
      scope: true,
      controller: function() {
        var vm = this;
        vm.namePattern = /^[a-zA-Z ]{1,25}$/;
      },
      controllerAs: 'vm',
      templateUrl: '/src/client/app/directives/name-field/last-name/last-name.html'
    }
  });
})();
