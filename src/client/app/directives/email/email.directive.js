/**
 * Created by Mark on 3/20/2017.
 */
(function () {
  'use strict';

  var app = angular.module('app.directives');

  // app.controller('EmailCtrl', function ($scope) {
  //   $scope.emailPattern = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
  // });

  app.directive('emailDirective', function () {
    return {
      restrict: 'AE',
      require:'ngModel',
      scope: true,
      templateUrl: '/src/client/app/directives/email/email.html',
      controller: function () {
        var vm = this;
        vm.emailPattern = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
      },
      controllerAs: 'vm'
    };

  });
})();
