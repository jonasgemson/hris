/**
 * Created by JR Adormeo on 23/03/2017.
 */
(function(){
  'use strict';

  var app = angular.module('app.directives');

  app.directive('mobileDirective2',function(){
    return{
      restrict:'EA',
      scope: true,
      templateUrl: '/src/client/app/directives/in-case-of-emergency/mobile-number/numberM.html',
      controller: function () {
        var vm=this;
        vm.mnumberOnly = /^[\+]?[(]?[0-9]{3}[)]?[-\s.]?[0-9]{3}[-\s.]?[0-9]{4,6}$/;
      },
      controllerAs: 'vm'
    };
  });

})();
