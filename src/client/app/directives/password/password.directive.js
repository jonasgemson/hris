/**
 * Created by JR Adormeo on 24/03/2017.
 */
(function(){
  'use strict';

  var app = angular.module('app.directives');

  app.directive('passwordDirective', function () {
    return{
      restrict: 'EA',
      bindToController: true,
      scope: true,
      controller: function () {
        var vm = this;
        vm.eyeicon = true;
        vm.showPassword = true;
        vm.countchange = 0;

        vm.showHidePassword = function () {
          vm.showPassword = !vm.showPassword;
          vm.eyeicon = !vm.eyeicon;
        };

      },
      controllerAs: 'vm',

      templateUrl: '/src/client/app/directives/password/password.html'
    };
  });

  app.directive("comparePass", function () {
    return {
      require: "ngModel",
      scope: {
        otherModelValue: "=comparePass"
      },
      link: function(scope, element, attributes, ngModel) {
        ngModel.$validators.comparePass = function(modelValue) {
          return modelValue == scope.otherModelValue;
        };
        scope.$watch("otherModelValue", function() {
          ngModel.$validate();
        });
      }
    };
  });


})();
