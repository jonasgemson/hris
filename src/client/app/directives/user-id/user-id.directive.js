/**
 * Created by Mark on 3/20/2017.
 */
(function () {
  'use strict';

  var app = angular.module('app.directives');

  app.directive('useridDirective', function () {
    return {
      restrict: 'AE',
      scope: true,
      controller: function() {
        var vm = this;
        vm.useridPattern = /^[a-zA-Z0-9-_]+$/;
      },
      controllerAs: 'vm',
      templateUrl: '/src/client/app/directives/user-id/user-id.html'
    }
  });
})();
