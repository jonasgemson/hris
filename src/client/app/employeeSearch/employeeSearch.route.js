(function() {
  'use strict';

  angular
    .module('app.employeeSearch')
    .run(appRun);

  appRun.$inject = ['routerHelper'];

  function appRun(routerHelper) {
    routerHelper.configureStates(getStates());
  }

  function getStates() {
    return [
      {
        state: 'employeeSearch',
        config: {
          url: '/employeeSearch',
          templateUrl: 'app/employeeSearch/employeeSearch.html',
          controller: 'employeeSearchController',
          controllerAs: 'vm',
          title: 'Employee Search',
          settings: {
            nav: 20,
            content: '<i class="fa fa-dashboard"></i> Employee Search'
          }
        }
      }
    ];
  }
})();
