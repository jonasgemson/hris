(function() {
  'use strict';

  angular.module('app.employeeSearch', [
    'app.core',
    'app.widgets'
  ]);
})();
