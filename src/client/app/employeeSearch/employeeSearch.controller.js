(function() {
  'use strict';

  angular
    .module('app.employeeSearch')
    .controller('employeeSearchController', employeeSearchController);

  employeeSearchController.$inject = ['$q', 'dataservice', 'logger', '$location'];
  /* @ngInject */
  function employeeSearchController($q, dataservice, logger, $location) {
    var vm = this;
    vm.people = [];
    activate();

    function activate() {
      var promises = [getPeople()];
      return $q.all(promises).then(function() {
        logger.info('Activated Dashboard View');
      });
    }

    function getPeople() {
      return dataservice.getPeople().then(function(data) {
        vm.people = data;
        return vm.people;
      });
    }



  }
})();
