(function() {
  'use strict';

  angular.module('app.myProfile', [
    'app.core',
    'app.widgets'
  ]);
})();
