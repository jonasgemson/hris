(function() {
  'use strict';

  angular
    .module('app.myProfile')
    .run(appRun);

  appRun.$inject = ['routerHelper'];

  function appRun(routerHelper) {
    routerHelper.configureStates(getStates());
  }

  function getStates() {
    return [
      {
        state: 'myProfile',
        config: {
          url: '/myProfile',
          templateUrl: 'app/profileInfo/myProfile/myProfile.html',
          controller: 'myProfileController',
          controllerAs: 'vms',
          title: 'myProfile',
          settings: {
            nav: 13,
            content: '<i class="fa fa-dashboard"></i> myProfile'
          }
        }
      }
    ];
  }
})();
