(function() {
  'use strict';

  angular.module('app.skillsDetails', [
    'app.core',
    'app.widgets'
  ]);
})();
