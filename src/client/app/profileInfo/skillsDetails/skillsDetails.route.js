(function() {
  'use strict';

  angular
    .module('app.skillsDetails')
    .run(appRun);

  appRun.$inject = ['routerHelper'];

  function appRun(routerHelper) {
    routerHelper.configureStates(getStates());
  }

  function getStates() {
    return [
      {
        state: 'skillsDetails',
        config: {
          url: '/skillsDetails',
          templateUrl: 'app/profileInfo/skillsDetails/skillsDetails.html',
          controller: 'skillsDetailsController',
          controllerAs: 'vms',
          title: 'skillsDetails',
          settings: {
            nav: 12,
            content: '<i class="fa fa-dashboard"></i> skillsDetails'
          }
        }
      },
      {
        state: 'skillsDetails.edit',
        config: {
          url: '/skillsDetailsEdit',
          templateUrl: 'app/profileInfo/skillsDetails/skillsDetailsEditTemplate.html',
          controller: 'skillsDetailsController',
          controllerAs: 'vms',
          title: 'skillsDetailsEdit',
          settings: {
            nav: 12,
            content: '<i class="fa fa-dashboard"></i> skillsDetailsEdit'
          }
        }
      },
      {
        state: 'skillsDetails.view',
        config: {
          url: '/skillsDetailsView',
          templateUrl: 'app/profileInfo/skillsDetails/skillsDetailsViewTemplate.html',
          controller: 'skillsDetailsController',
          controllerAs: 'vms',
          title: 'skillsDetailsView',
          settings: {
            nav: 12,
            content: '<i class="fa fa-dashboard"></i> skillsDetailsView'
          }
        }
      }



    ];
  }
})();
