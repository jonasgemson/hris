(function() {
  'use strict';

  angular.module('app.seminarsAndTrainings', [
    'app.core',
    'app.widgets'
  ]);
})();
