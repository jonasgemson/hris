(function() {
  'use strict';

  angular
    .module('app.seminarsAndTrainings')
    .run(appRun);

  appRun.$inject = ['routerHelper'];

  function appRun(routerHelper) {
    routerHelper.configureStates(getStates());
  }

  function getStates() {
    return [
      {
        state: 'seminarsAndTrainings',
        config: {
          url: '/seminarsAndTrainings',
          templateUrl: 'app/profileInfo/seminarsAndTrainings/seminarsAndTrainings.html',
          controller: 'seminarsAndTrainingsController',
          controllerAs: 'vms',
          title: 'seminarsAndTrainings',
          settings: {
            nav: 10,
            content: '<i class="fa fa-dashboard"></i> seminarsAndTrainings'
          }
        }
      },
      {
        state: 'seminarsAndTrainings.edit',
        config: {
          url: '/seminarsAndTrainingsEdit',
          templateUrl: 'app/profileInfo/seminarsAndTrainings/seminarsAndTrainingsEditTemplate.html',
          controller: 'seminarsAndTrainingsController',
          controllerAs: 'vms',
          title: 'seminarsAndTrainingsEdit',
          settings: {
            nav: 11,
            content: '<i class="fa fa-dashboard"></i> seminarsAndTrainings'
          }
        }
      },
      {
        state: 'seminarsAndTrainings.view',
        config: {
          url: '/seminarsAndTrainingsView',
          templateUrl: 'app/profileInfo/seminarsAndTrainings/seminarsAndTrainingsViewTemplate.html',
          controller: 'seminarsAndTrainingsController',
          controllerAs: 'vms',
          title: 'seminarsAndTrainingsView',
          settings: {
            nav: 11,
            content: '<i class="fa fa-dashboard"></i> seminarsAndTrainingsView'
          }
        }
      }

    ];
  }
})();
