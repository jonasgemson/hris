(function() {
  'use strict';

  angular.module('app.educationDetails', [
    'app.core',
    'app.widgets'
  ]);
})();
