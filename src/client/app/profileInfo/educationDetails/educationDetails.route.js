(function() {
  'use strict';

  angular
    .module('app.educationDetails')
    .run(appRun);

  appRun.$inject = ['routerHelper'];

  function appRun(routerHelper) {
    routerHelper.configureStates(getStates());
  }

  function getStates() {
    return [
      {
        state: 'educationDetails',
        config: {
          url: '/educationDetails',
          templateUrl: 'app/profileInfo/educationDetails/educationDetails.html',
          controller: 'educationDetailsController',
          controllerAs: 'vm',
          title: 'educationDetails',
          settings: {
            nav: 6,
            content: '<i class="fa fa-dashboard"></i> EducationDetails'
          }
        }
      },
      {
        state: 'educationDetails.edit',
        config: {
          url: '/educationDetailsEdit',
          templateUrl: 'app/profileInfo/educationDetails/educationDetailsEditTemplate.html',
          controller: 'educationDetailsController',
          controllerAs: 'vm',
          title: 'educationDetailsEdit',
          settings: {
            nav: 6,
            content: '<i class="fa fa-dashboard"></i> EducationDetailsEdit'
          }
        }
      },
      {
        state: 'educationDetails.view',
        config: {
          url: '/educationDetailsView',
          templateUrl: 'app/profileInfo/educationDetails/educationDetailsViewTemplate.html',
          controller: 'educationDetailsController',
          controllerAs: 'vm',
          title: 'educationDetailsView',
          settings: {
            nav: 6,
            content: '<i class="fa fa-dashboard"></i> EducationDetailsView'
          }
        }
      }


    ];
  }
})();
