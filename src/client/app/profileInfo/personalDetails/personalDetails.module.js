(function() {
  'use strict';

  angular.module('app.personalDetails', [
    'app.core',
    'app.widgets'
  ]);
})();
