(function() {
  'use strict';

  angular
    .module('app.personalDetails')
    .controller('personalDetailsController', personalDetailsController);

  personalDetailsController.$inject = ['$q', 'dataservice', 'logger', '$location'];
  /* @ngInject */
  function personalDetailsController($q, dataservice, logger, $location) {
    console.log('Personal');
    var vm = this;
    vm.people = [];
    activate();

    function activate() {
      var promises = [getPeople()];
      return $q.all(promises).then(function() {
        logger.info('Activated Dashboard View');
      });
    }

    function getPeople() {
      return dataservice.getPeople().then(function(data) {
        vm.people = data;
        return vm.people;
      });
    }



  }
})();
