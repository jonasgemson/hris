(function() {
  'use strict';

  angular
    .module('app.personalDetails')
    .run(appRun);

  appRun.$inject = ['routerHelper'];

  function appRun(routerHelper) {
    routerHelper.configureStates(getStates());
  }

  function getStates() {
    return [
      {
        state: 'personalDetails',
        config: {
          url: '/personalDetails',
          templateUrl: 'app/profileInfo/personalDetails/personalDetails.html',
          controller: 'personalDetailsController',
          controllerAs: 'vm',
          title: 'personalDetails',
          settings: {
            nav: 8,
            content: '<i class="fa fa-dashboard"></i> personalDetails'
          }
        }
      },
      {
        state: 'personalDetails.edit',
        config: {
          url: '/personalDetailsEdit',
          templateUrl: 'app/profileInfo/personalDetails/personalEditTemplate.html',
          controller: 'personalDetailsController',
          controllerAs: 'vm',
          title: 'personalDetailsEdit',
          settings: {
            nav: 9,
            content: '<i class="fa fa-dashboard"></i> personalDetails'
          }
        }
      },
      {
        state: 'personalDetails.view',
        config: {
          url: '/personalDetailsView',
          templateUrl: 'app/profileInfo/personalDetails/personalViewTemplate.html',
          controller: 'personalDetailsController',
          controllerAs: 'vm',
          title: 'Personal Details',
          settings: {
            nav: 9,
            content: '<i class="fa fa-dashboard"></i> personalViewDetails'
          }
        }
      }

    ];
  }
})();
