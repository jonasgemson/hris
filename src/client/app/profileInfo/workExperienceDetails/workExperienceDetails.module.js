(function() {
  'use strict';

  angular.module('app.workExperienceDetails', [
    'app.core',
    'app.widgets'
  ]);
})();
