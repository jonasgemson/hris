(function() {
  'use strict';

  angular
    .module('app.workExperienceDetails')
    .run(appRun);

  appRun.$inject = ['routerHelper'];

  function appRun(routerHelper) {
    routerHelper.configureStates(getStates());
  }

  function getStates() {
    return [
      {
        state: 'workExperienceDetails',
        config: {
          url: '/workExperienceDetails',
          templateUrl: 'app/profileInfo/workExperienceDetails/workExperienceDetails.html',
          controller: 'workExperienceDetailsController',
          controllerAs: 'vms',
          title: 'workExperienceDetails',
          settings: {
            nav: 14,
            content: '<i class="fa fa-dashboard"></i> workExperienceDetails'
          }
        }
      },
      {
        state: 'workExperienceDetails.edit',
        config: {
          url: '/workExperienceDetailsEdit',
          templateUrl: 'app/profileInfo/workExperienceDetails/workExperienceDetailsEditTemplate.html',
          controller: 'workExperienceDetailsController',
          controllerAs: 'vms',
          title: 'workExperienceDetailsEdit',
          settings: {
            nav: 15,
            content: '<i class="fa fa-dashboard"></i> workExperienceDetails'
          }
        }
      },
      {
        state: 'workExperienceDetails.view',
        config: {
          url: '/workExperienceDetailsView',
          templateUrl: 'app/profileInfo/workExperienceDetails/workExperienceDetailsViewTemplate.html',
          controller: 'workExperienceDetailsController',
          controllerAs: 'vms',
          title: 'workExperienceDetailsView',
          settings: {
            nav: 15,
            content: '<i class="fa fa-dashboard"></i> workExperienceDetailsView'
          }
        }
      }

    ];
  }
})();
