(function() {
  'use strict';

  angular.module('app', [
    'app.core',
    'app.widgets',
    'app.admin',
    'app.dashboard',
    'app.layout',
    'app.login',
    'app.educationDetails',
    'app.personalDetails',
    'app.seminarsAndTrainings',
    'app.skillsDetails',
    'app.workExperienceDetails',
    'app.directives',
    'app.myProfile',
    'app.advanceSearch',
    'app.employeeSearch'
  ]);

})();
